

module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('tbl_comics', [
      {
        image: '/images/users/default.png',
        name: 'Naruto',
        slug: 'naruto-1',
        otherName: "['Naruto Shippuden','Naruto']",
        authorId: 1,
        artistId: 1,
        releasedYear: 1998,
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'One Piece',
        slug: 'one-piece-1',
        otherName: "['One Piece','OP']",
        authorId: 1,
        artistId: 1,
        releasedYear: 1998,
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'Naruto',
        slug: 'naruto-2',
        otherName: "['Naruto Shippuden','Naruto']",
        authorId: 1,
        artistId: 1,
        releasedYear: 1998,
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'One Piece',
        slug: 'one-piece-2',
        otherName: "['One Piece','OP']",
        authorId: 1,
        artistId: 1,
        releasedYear: 1998,
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'Naruto',
        slug: 'naruto-3',
        otherName: "['Naruto Shippuden','Naruto']",
        authorId: 1,
        artistId: 1,
        releasedYear: 1998,
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'One Piece',
        slug: 'one-piece-3',
        otherName: "['One Piece','OP']",
        authorId: 1,
        artistId: 1,
        releasedYear: 1998,
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'Naruto',
        slug: 'naruto-4',
        otherName: "['Naruto Shippuden','Naruto']",
        authorId: 1,
        artistId: 1,
        releasedYear: 1998,
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'One Piece',
        slug: 'one-piece-4',
        otherName: "['One Piece','OP']",
        authorId: 1,
        artistId: 1,
        releasedYear: 1998,
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'Naruto',
        slug: 'naruto-5',
        otherName: "['Naruto Shippuden','Naruto']",
        authorId: 1,
        artistId: 1,
        releasedYear: 1998,
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'One Piece',
        slug: 'one-piece-5',
        otherName: "['One Piece','OP']",
        authorId: 1,
        artistId: 1,
        releasedYear: 1998,
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'Naruto',
        slug: 'naruto-6',
        otherName: "['Naruto Shippuden','Naruto']",
        authorId: 1,
        artistId: 1,
        releasedYear: 1998,
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'One Piece',
        slug: 'one-piece-6',
        otherName: "['One Piece','OP']",
        authorId: 1,
        artistId: 1,
        releasedYear: 1998,
        createdBy: 1,
        createdAt: new Date()
      }
    ])
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('tbl_comics', null, {})
  }
}
