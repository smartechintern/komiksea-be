

module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('tbl_artist', [
      {
        image: '/images/users/default.png',
        name: 'Eiichiro Oda1',
        slug: 'eiichiro-oda1',
        description: 'Artist and Author of One Piece',
        birthDate: new Date(1975, 1, 1),
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'Eiichiro Oda2',
        slug: 'eiichiro-oda2',
        description: 'Artist and Author of One Piece',
        birthDate: new Date(1975, 1, 1),
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'Eiichiro Oda3',
        slug: 'eiichiro-oda3',
        description: 'Artist and Author of One Piece',
        birthDate: new Date(1975, 1, 1),
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'Eiichiro Oda4',
        slug: 'eiichiro-oda4',
        description: 'Artist and Author of One Piece',
        birthDate: new Date(1975, 1, 1),
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'Eiichiro Oda5',
        slug: 'eiichiro-oda5',
        description: 'Artist and Author of One Piece',
        birthDate: new Date(1975, 1, 1),
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'Eiichiro Oda6',
        slug: 'eiichiro-oda6',
        description: 'Artist and Author of One Piece',
        birthDate: new Date(1975, 1, 1),
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'Eiichiro Oda7',
        slug: 'eiichiro-oda7',
        description: 'Artist and Author of One Piece',
        birthDate: new Date(1975, 1, 1),
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'Eiichiro Oda8',
        slug: 'eiichiro-oda8',
        description: 'Artist and Author of One Piece',
        birthDate: new Date(1975, 1, 1),
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'Eiichiro Oda9',
        slug: 'eiichiro-oda9',
        description: 'Artist and Author of One Piece',
        birthDate: new Date(1975, 1, 1),
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'Eiichiro Oda10',
        slug: 'eiichiro-oda10',
        description: 'Artist and Author of One Piece',
        birthDate: new Date(1975, 1, 1),
        createdBy: 1,
        createdAt: new Date()
      },
      {
        image: '/images/users/default.png',
        name: 'Eiichiro Oda11',
        slug: 'eiichiro-oda11',
        description: 'Artist and Author of One Piece',
        birthDate: new Date(1975, 1, 1),
        createdBy: 1,
        createdAt: new Date()
      }
    ])
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('tbl_artist', null, {})
  }
}
