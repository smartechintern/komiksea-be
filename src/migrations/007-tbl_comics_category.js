module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('tbl_comics_category', {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER.UNSIGNED
      },
      comicsId: {
        allowNull: false,
        references: {
          model: 'tbl_comics',
          key: 'id'
        },
        onUpdate: 'RESTRICT',
        onDeleted: 'RESTRICT',
        type: Sequelize.INTEGER.UNSIGNED
      },
      categoryId: {
        allowNull: false,
        references: {
          model: 'tbl_category',
          key: 'id'
        },
        onUpdate: 'RESTRICT',
        onDeleted: 'RESTRICT',
        type: Sequelize.INTEGER.UNSIGNED
      },
      createdBy: {
        allowNull: false,
        references: {
          model: 'tbl_user',
          key: 'id'
        },
        onUpdate: 'RESTRICT',
        onDeleted: 'RESTRICT',
        type: Sequelize.INTEGER.UNSIGNED
      },
      updatedBy: {
        allowNull: true,
        defaultValue: null,
        references: {
          model: 'tbl_user',
          key: 'id'
        },
        onUpdate: 'RESTRICT',
        onDeleted: 'RESTRICT',
        type: Sequelize.INTEGER.UNSIGNED
      },
      deletedBy: {
        allowNull: true,
        defaultValue: null,
        references: {
          model: 'tbl_user',
          key: 'id'
        },
        onUpdate: 'RESTRICT',
        onDeleted: 'RESTRICT',
        type: Sequelize.INTEGER.UNSIGNED
      },
      createdAt: {
        allowNull: false,
        defaultValue: Sequelize.NOW,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        defaultValue: null,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        defaultValue: null,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('tbl_comics_category')
  }
}
