module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('tbl_comics', {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER.UNSIGNED
      },
      image: {
        allowNull: true,
        type: Sequelize.STRING(191)
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING(191)
      },
      slug: {
        allowNull: false,
        unique: true,
        type: Sequelize.STRING(191)
      },
      otherName: {
        allowNull: true,
        type: Sequelize.STRING(191)
      },
      authorId: {
        allowNull: false,
        references: {
          model: 'tbl_author',
          key: 'id'
        },
        onUpdate: 'RESTRICT',
        onDeleted: 'RESTRICT',
        type: Sequelize.INTEGER.UNSIGNED
      },
      artistId: {
        allowNull: false,
        references: {
          model: 'tbl_artist',
          key: 'id'
        },
        onUpdate: 'RESTRICT',
        onDeleted: 'RESTRICT',
        type: Sequelize.INTEGER.UNSIGNED
      },
      releasedYear: {
        allowNull: false,
        type: Sequelize.INTEGER.UNSIGNED
      },
      createdBy: {
        allowNull: false,
        references: {
          model: 'tbl_user',
          key: 'id'
        },
        onUpdate: 'RESTRICT',
        onDeleted: 'RESTRICT',
        type: Sequelize.INTEGER.UNSIGNED
      },
      updatedBy: {
        allowNull: true,
        defaultValue: null,
        references: {
          model: 'tbl_user',
          key: 'id'
        },
        onUpdate: 'RESTRICT',
        onDeleted: 'RESTRICT',
        type: Sequelize.INTEGER.UNSIGNED
      },
      deletedBy: {
        allowNull: true,
        defaultValue: null,
        references: {
          model: 'tbl_user',
          key: 'id'
        },
        onUpdate: 'RESTRICT',
        onDeleted: 'RESTRICT',
        type: Sequelize.INTEGER.UNSIGNED
      },
      createdAt: {
        allowNull: false,
        defaultValue: Sequelize.NOW,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        defaultValue: null,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        defaultValue: null,
        type: Sequelize.DATE
      }
    })
      .then(() => queryInterface.addIndex('tbl_comics', ['name']))
      .then(() => queryInterface.addIndex('tbl_comics', ['otherName']))
      .then(() => queryInterface.addIndex('tbl_comics', ['authorId']))
      .then(() => queryInterface.addIndex('tbl_comics', ['artistId']))
      .then(() => queryInterface.addIndex('tbl_comics', ['slug']))
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('tbl_comics')
  }
}
