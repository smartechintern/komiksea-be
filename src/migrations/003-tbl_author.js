module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('tbl_author', {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER.UNSIGNED
      },
      image: {
        allowNull: true,
        type: Sequelize.STRING(191)
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING(191)
      },
      slug: {
        allowNull: false,
        type: Sequelize.STRING(191)
      },
      description: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      birthDate: {
        allowNull: true,
        type: Sequelize.DATEONLY
      },
      createdBy: {
        allowNull: false,
        references: {
          model: 'tbl_user',
          key: 'id'
        },
        onUpdate: 'RESTRICT',
        onDeleted: 'RESTRICT',
        type: Sequelize.INTEGER.UNSIGNED
      },
      updatedBy: {
        allowNull: true,
        defaultValue: null,
        references: {
          model: 'tbl_user',
          key: 'id'
        },
        onUpdate: 'RESTRICT',
        onDeleted: 'RESTRICT',
        type: Sequelize.INTEGER.UNSIGNED
      },
      deletedBy: {
        allowNull: true,
        defaultValue: null,
        references: {
          model: 'tbl_user',
          key: 'id'
        },
        onUpdate: 'RESTRICT',
        onDeleted: 'RESTRICT',
        type: Sequelize.INTEGER.UNSIGNED
      },
      createdAt: {
        allowNull: false,
        defaultValue: Sequelize.NOW,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        defaultValue: null,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        defaultValue: null,
        type: Sequelize.DATE
      }
    })
      .then(() => queryInterface.addIndex('tbl_author', ['name']))
      .then(() => queryInterface.addIndex('tbl_author', ['slug']))
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('tbl_author')
  }
}
