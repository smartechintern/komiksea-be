module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('tbl_user', {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER.UNSIGNED
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING(191)
      },
      email: {
        unique: true,
        allowNull: false,
        type: Sequelize.STRING(191)
      },
      avatar: {
        allowNull: true,
        defaultValue: 'users/default.png',
        type: Sequelize.STRING(191)
      },
      password: {
        allowNull: false,
        type: Sequelize.STRING(191)
      },
      remember_token: {
        allowNull: true,
        type: Sequelize.STRING(100)
      },
      role: {
        allowNull: true,
        type: Sequelize.STRING(30)
      },
      createdBy: {
        allowNull: true,
        defaultValue: null,
        references: {
          model: 'tbl_user',
          key: 'id',
          as: 'createdBy'
        },
        onUpdate: 'RESTRICT',
        onDeleted: 'RESTRICT',
        type: Sequelize.INTEGER.UNSIGNED
      },
      updatedBy: {
        allowNull: true,
        defaultValue: null,
        references: {
          model: 'tbl_user',
          key: 'id',
          as: 'updatedBy'
        },
        onUpdate: 'RESTRICT',
        onDeleted: 'RESTRICT',
        type: Sequelize.INTEGER.UNSIGNED
      },
      deletedBy: {
        allowNull: true,
        defaultValue: null,
        references: {
          model: 'tbl_user',
          key: 'id',
          as: 'deletedBy'
        },
        onUpdate: 'RESTRICT',
        onDeleted: 'RESTRICT',
        type: Sequelize.INTEGER.UNSIGNED
      },
      createdAt: {
        allowNull: true,
        defaultValue: Sequelize.NOW,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        defaultValue: null,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        defaultValue: null,
        type: Sequelize.DATE
      }
    })
      .then(() => queryInterface.addIndex('tbl_user', ['email', 'password']))
      .then(() => queryInterface.addIndex('tbl_user', ['email']))
      .then(() => queryInterface.addIndex('tbl_user', ['name']))
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('tbl_user')
  }
}
