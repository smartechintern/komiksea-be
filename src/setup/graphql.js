// Imports
import graphqlHTTP from 'express-graphql'

// App Imports
import serverConfig from '../config/server.json'
import authentication from './authentication'
import schema from './schema'

import { errorType } from '../constants'

const getErrorCode = (errorName) => {
  return errorType[errorName]
}

// Setup GraphQL
export default function (server) {
  console.info('SETUP - GraphQL...')

  server.use(authentication)

  // API (GraphQL on route `/`)
  server.use(serverConfig.graphql.endpoint, graphqlHTTP(async request => ({
    schema,
    graphiql: process.env.NODE_ENV !== 'production',
    pretty: serverConfig.graphql.pretty,
    context: {
      auth: {
        user: request.user,
        isAuthenticated: request.user && request.user.id > 0
      }
    },
    formatError: (err) => {
      const error = getErrorCode(err.message) || {}
      return ({
        message: error.message || err.message,
        statusCode: error.statusCode || 404
      })
    }
  })))
}
