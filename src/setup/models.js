// Imports
import Sequelize from 'sequelize'

// App Imports
import databaseConnection from './database'

const models = {
  User: databaseConnection.import('../modules/user/model'),
  Product: databaseConnection.import('../modules/komik/model'),
  Comics: databaseConnection.import('../modules/comics/model'),
  Subscription: databaseConnection.import('../modules/subscription/model'),
  Artist: databaseConnection.import('../modules/artist/model')
}

Object.keys(models).forEach((modelName) => {
  if (models[modelName].associate) {
    models[modelName].associate(models)
  }
})

models.sequelize = databaseConnection
models.Sequelize = Sequelize

export default models
