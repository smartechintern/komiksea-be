// Imports
import { GraphQLObjectType } from 'graphql'

// App Imports
import * as user from '../../modules/user/query'
import * as komik from '../../modules/komik/query'
import * as comics from '../../modules/comics/query'
import * as subscription from '../../modules/subscription/query'
import * as artist from '../../modules/artist/query'

// Query
const query = new GraphQLObjectType({
  name: 'query',
  description: 'API Queries [Read]',

  fields: () => ({
    ...user,
    ...komik,
    ...comics,
    ...subscription,
    ...artist
  })
})

export default query
