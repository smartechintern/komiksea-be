// Imports
import { GraphQLObjectType } from 'graphql'

// App Imports
import * as user from '../../modules/user/mutations'
import * as product from '../../modules/komik/mutations'
import * as comics from '../../modules/comics/mutations'
import * as subscription from '../../modules/subscription/mutations'
import * as artist from '../../modules/artist/mutations'

// Mutation
const mutation = new GraphQLObjectType({
  name: 'mutations',
  description: 'API Mutations [Create, Update, Delete]',

  fields: {
    ...user,
    ...product,
    ...comics,
    ...subscription,
    ...artist
  }
})

export default mutation
