// Imports
import path from 'path'
import multer from 'multer'

// App Imports
import serverConfig from '../config/server.json'

const objDate = new Date()
const locale = 'en-us'
const getDir = `${(`0${objDate.getDate()}`).slice(-2)}${objDate.toLocaleString(
  locale,
  { month: 'long' }
)}${objDate.getFullYear(2).toString()}`

// File upload configurations and route
export default function (server) {
  console.info('SETUP - Upload...')

  // Set destination
  const storage = multer.diskStorage({
    destination: path.join(__dirname, '..', '..', 'public', 'images', 'uploads', getDir),

    filename (request, file, callback) {
      callback(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`)
    }
  })

  const upload = multer({
    storage
  }).single('file')

  // Upload route
  server.post(serverConfig.upload.endpoint, (request, response) => {
    upload(request, response, (error) => {
      if (!error) {
        response.json({
          success: true,
          file: `${serverConfig.upload.endpoint}/${getDir}/${request.file.filename}`
        })
      } else {
        response.json({
          success: false,
          file: null,
          error
        })
      }
    })
  })
}
