import Sequelize from 'sequelize'
import databaseConfig from '../config/database.json'
import { NODE_ENV } from '../config/env'

const databaseConfigEnv = databaseConfig[NODE_ENV]

const sequelize = new Sequelize(databaseConfigEnv.database, databaseConfigEnv.username, databaseConfigEnv.password,
  {
    host: databaseConfigEnv.host,
    dialect: databaseConfigEnv.dialect,
    logging: false,
    operatorsAliases: Sequelize.Op,
    define: {
      timezone: databaseConfigEnv.define.timestamps,
      paranoid: databaseConfigEnv.define.paranoid
    }
  })

module.exports = sequelize
