exports.errorName = {
  UNAUTHORIZED: 'UNAUTHORIZED'
}

exports.errorType = {
  UNAUTHORIZED: {
    message: 'Please Login',
    statusCode: 401
  }
}
