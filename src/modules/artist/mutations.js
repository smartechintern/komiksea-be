// Imports
import { GraphQLString, GraphQLInt } from 'graphql'

// App Imports
import Artist from './types'
import { create, update, remove } from './resolvers'

// Artist create
export const artistCreate = {
  type: Artist,
  args: {
    image: {
      name: 'image',
      type: GraphQLString
    },

    name: {
      name: 'name',
      type: GraphQLString
    },

    slug: {
      name: 'slug',
      type: GraphQLString
    },

    description: {
      name: 'description',
      type: GraphQLString
    },

    birthDate: {
      name: 'birthDate',
      type: GraphQLString
    }

  },
  resolve: create
}

// Artist update
export const artistUpdate = {
  type: Artist,
  args: {
    id: {
      name: 'id',
      type: GraphQLInt
    },

    image: {
      name: 'image',
      type: GraphQLString
    },

    name: {
      name: 'name',
      type: GraphQLString
    },

    slug: {
      name: 'slug',
      type: GraphQLString
    },

    description: {
      name: 'description',
      type: GraphQLString
    },

    birthDate: {
      name: 'birthDate',
      type: GraphQLString
    }
  },
  resolve: update
}
// Artist remove
export const artistRemove = {

  type: Artist,
  args: {
    id: {
      name: 'id',
      type: GraphQLInt
    }
  },
  resolve: remove
}
