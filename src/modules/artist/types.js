// Imports
import { GraphQLObjectType, GraphQLString, GraphQLInt } from 'graphql'

// Artist
const Artist = new GraphQLObjectType({
  name: 'artist',
  description: 'Artist',

  fields: () => ({
    id: { type: GraphQLInt },
    image: { type: GraphQLString },
    name: { type: GraphQLString },
    description: { type: GraphQLString },
    slug: { type: GraphQLString },
    birthDate: { type: GraphQLString },
    createdBy: { type: GraphQLString },
    updatedBy: { type: GraphQLString },
    deletedBy: { type: GraphQLString },
    createdAt: { type: GraphQLString },
    updatedAt: { type: GraphQLString },
    deletedAt: { type: GraphQLString }
  })
})

export default Artist
