// Imports
import { GraphQLString, GraphQLInt, GraphQLList } from 'graphql'

// App Imports
import Artist from './types'
import { getAll, getBySlug, getById } from './resolvers'

// Artist All
export const artistQuery = {
  type: new GraphQLList(Artist),
  args: {
    q: { type: GraphQLString },
    order: { type: GraphQLString },
    page: { type: GraphQLInt },
    pageSize: { type: GraphQLInt },
    type: { type: GraphQLString }
  },
  resolve: getAll
}

// Artist By slug
export const artistBySlug = {
  type: Artist,
  args: {
    slug: { type: GraphQLString }
  },
  resolve: getBySlug
}

// Artist By ID
export const artistById = {
  type: Artist,
  args: {
    id: { type: GraphQLInt }
  },
  resolve: getById
}
