// App Imports
import Sequelize from 'sequelize'
import params from '../../config/params'
import models from '../../setup/models'
import sequelize from '../../native/sequelize'

const { Op } = Sequelize

// Get all artist
export async function getAll (parentValue, {
  q, order, page, pageSize
}) {
  const data = await models.Artist.findAll({
    where: {
      [Op.or]: [
        {
          name: {
            [Op.like]: `%${q}%`
          }
        }
      ]
    },
    order: order ? sequelize.literal(order) : [['id', 'DESC']],
    limit: parseInt(pageSize || 10, 10),
    offset: parseInt(page - 1 || 0, 0) * parseInt(pageSize || 10, 10)
  })

  return data
}

// Get by slug
export async function getBySlug (parentValue, { slug }) {
  const artist = await models.Artist.findOne({ where: { slug } })

  if (!artist) {
    // does not exists
    throw new Error('The artist you are looking for does not exists or has been discontinued.')
  } else {
    return artist
  }
}

// Get by ID
export async function getById (parentValue, { id }) {
  const artist = await models.Artist.findOne({ where: { id } })

  if (!artist) {
    // Artist does not exists
    throw new Error('The artist you are looking for does not exists or has been discontinued.')
  } else {
    return artist
  }
}

// Create
export async function create (parentValue, {
  image, name, slug, description, birthDate
}, { auth }) {
  if ((process.env.NODE_ENV === 'development') || (auth.user && auth.user.role === params.user.roles.admin)) {
    const data = await models.Artist.create({
      image,
      name,
      slug,
      description,
      birthDate,
      createdBy: auth.user.id
    })

    return data
  }
  throw new Error('Operation denied.')
}

// Update
export async function update (parentValue, {
  id, image, name, slug, description, birthDate
}, { auth }) {
  if (auth.user && auth.user.role === params.user.roles.admin) {
    const data = await models.Artist.update(
      {
        image,
        name,
        description,
        slug,
        birthDate,
        updatedBy: auth.user.id
      },
      { where: { id } }
    )

    return data
  }
  throw new Error('Operation denied.')
}

// Delete
export async function remove (parentValue, { id }, { auth }) {
  if (auth.user && auth.user.role === params.user.roles.admin) {
    const artist = await models.Artist.findOne({ where: { id } })

    if (!artist) {
      // Artist does not exists
      throw new Error('The artist does not exists.')
    } else {
      const data = await models.Artist.destroy({ where: { id } })
      return data
    }
  } else {
    throw new Error('Operation denied.')
  }
}
