

// Product
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('tbl_artist', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    image: {
      allowNull: true,
      type: DataTypes.STRING(191)
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING(191)
    },
    slug: {
      allowNull: false,
      type: DataTypes.STRING(191)
    },
    description: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    birthDate: {
      allowNull: true,
      type: DataTypes.DATEONLY
    },
    createdBy: {
      allowNull: false,
      references: {
        model: 'tbl_user',
        key: 'id'
      },
      onUpdate: 'RESTRICT',
      onDeleted: 'RESTRICT',
      type: DataTypes.INTEGER.UNSIGNED
    },
    updatedBy: {
      allowNull: true,
      defaultValue: null,
      references: {
        model: 'tbl_user',
        key: 'id'
      },
      onUpdate: 'RESTRICT',
      onDeleted: 'RESTRICT',
      type: DataTypes.INTEGER.UNSIGNED
    },
    deletedBy: {
      allowNull: true,
      defaultValue: null,
      references: {
        model: 'tbl_user',
        key: 'id'
      },
      onUpdate: 'RESTRICT',
      onDeleted: 'RESTRICT',
      type: DataTypes.INTEGER.UNSIGNED
    },
    createdAt: {
      allowNull: false,
      defaultValue: DataTypes.NOW,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.DATE
    },
    deletedAt: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.DATE
    }
  }, {
    timestamps: true,
    tableName: 'tbl_artist',
    paranoid: true
  })
}
