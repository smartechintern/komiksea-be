// App Imports
import models from '../../setup/models'

// Get subscription by ID
export async function get (parentValue, { id }) {
  const data = await models.Subscription.findOne({
    where: { id },
    include: [
      { model: models.User, as: 'user' }
    ]
  })
  return data
}

// Get subscription by user
export async function getByUser (parentValue, { auth }) {
  if (auth.user && auth.user.id > 0) {
    const data = await models.Subscription.findAll({
      where: {
        userId: auth.user.id
      },
      include: [
        { model: models.User, as: 'user' }
      ]
    })

    return data
  }
  throw new Error('Please login to view your subscriptions.')
}

// Get all subscriptions
export async function getAll () {
  const data = await models.Subscription.findAll({
    include: [
      { model: models.User, as: 'user' }
    ]
  })

  return data
}

// Create subscription
export async function create (parentValue, { crateId }, { auth }) {
  if (auth.user && auth.user.id > 0) {
    const data = await models.Subscription.create({
      crateId,
      userId: auth.user.id
    })

    return data
  }
  throw new Error('Please login to subscribe to this crate.')
}

// Delete subscription
export async function remove (parentValue, { id }, { auth }) {
  if (auth.user && auth.user.id > 0) {
    const data = await models.Subscription.destroy({ where: { id, userId: auth.user.id } })
    return data
  }
  throw new Error('Access denied.')
}
