

// Subscription
module.exports = function (sequelize, DataTypes) {
  let Subscription = sequelize.define('subscriptions', {
    userId: {
      type: DataTypes.INTEGER
    },
    crateId: {
      type: DataTypes.INTEGER
    }
  }, {
    timestamps: true
  })

  Subscription.associate = function (models) {
    Subscription.belongsTo(models.User)
  }

  return Subscription
}
