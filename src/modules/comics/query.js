// Imports
import { GraphQLString, GraphQLInt, GraphQLList } from 'graphql'

// App Imports
import Comics from './types'
import { getAll, getBySlug, getById } from './resolvers'

// Comics All
export const comicsQuery = {
  type: new GraphQLList(Comics),
  args: {
    q: { type: GraphQLString },
    order: { type: GraphQLString },
    page: { type: GraphQLInt },
    pageSize: { type: GraphQLInt },
    type: { type: GraphQLString }
  },
  resolve: getAll
}

// Comics By slug
export const comicsBySlug = {
  type: Comics,
  args: {
    slug: { type: GraphQLString }
  },
  resolve: getBySlug
}

// Comics By ID
export const comicsById = {
  type: Comics,
  args: {
    id: { type: GraphQLInt }
  },
  resolve: getById
}
