// Imports
import { GraphQLObjectType, GraphQLString, GraphQLInt } from 'graphql'

// Comics
const Artist = new GraphQLObjectType({
  name: 'comics',
  description: 'Comics',

  fields: () => ({
    id: { type: GraphQLInt },
    image: { type: GraphQLString },
    name: { type: GraphQLString },
    slug: { type: GraphQLString },
    otherName: { type: GraphQLString },
    authorId: { type: GraphQLInt },
    artistId: { type: GraphQLInt },
    releasedYear: { type: GraphQLInt },
    createdBy: { type: GraphQLString },
    updatedBy: { type: GraphQLString },
    deletedBy: { type: GraphQLString },
    createdAt: { type: GraphQLString },
    updatedAt: { type: GraphQLString },
    deletedAt: { type: GraphQLString }
  })
})

export default Artist
