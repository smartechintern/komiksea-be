// App Imports
import Sequelize from 'sequelize'
import params from '../../config/params'
import models from '../../setup/models'
import sequelize from '../../native/sequelize'

const { Op } = Sequelize

// Get all comics
export async function getAll (parentValue, {
  q, order, page, pageSize
}) {
  const data = await models.Comics.findAll({
    where: {
      [Op.or]: [
        {
          name: {
            [Op.like]: `%${q}%`
          }
        }
      ]
    },
    order: order ? sequelize.literal(order) : [['id', 'DESC']],
    limit: parseInt(pageSize || 10, 10),
    offset: parseInt(page - 1 || 0, 0) * parseInt(pageSize || 10, 10)
  })

  return data
}

// Get by slug
export async function getBySlug (parentValue, { slug }) {
  const data = await models.Comics.findOne({ where: { slug } })

  if (!data) {
    // does not exists
    throw new Error('The comics you are looking for does not exists or has been discontinued.')
  } else {
    return data
  }
}

// Get by ID
export async function getById (parentValue, { id }) {
  const data = await models.Comics.findOne({ where: { id } })

  if (!data) {
    // Comics does not exists
    throw new Error('The comics you are looking for does not exists or has been discontinued.')
  } else {
    return data
  }
}

// Create
export async function create (parentValue, dataValues, { auth }) {
  if ((process.env.NODE_ENV === 'development') || (auth.user && auth.user.role === params.user.roles.admin)) {
    dataValues.createdBy = auth.user.id
    const data = await models.Comics.create(
      dataValues,
      {
        fields: [
          'image',
          'name',
          'slug',
          'description',
          'birthDate',
          'createdBy'
        ]
      }
    )
    return data
  }
  throw new Error('Operation denied.')
}

// Update
export async function update (parentValue, dataValues, { auth }) {
  if (auth.user && auth.user.role === params.user.roles.admin) {
    dataValues.updatedBy = auth.user.id
    const data = await models.Comics.update(
      dataValues,
      {
        fields: [
          'id',
          'image',
          'name',
          'slug',
          'otherName',
          'authorId',
          'artistId',
          'releasedYear',
          'updatedBy'
        ],
        where: { id: dataValues.id }
      }
    )

    return data
  }
  throw new Error('Operation denied.')
}

// Delete
export async function remove (parentValue, { id }, { auth }) {
  if (auth.user && auth.user.role === params.user.roles.admin) {
    const comics = await models.Comics.findOne({ where: { id } })

    if (!comics) {
      // Artist does not exists
      throw new Error('The comics does not exists.')
    } else {
      const data = await models.Artist.destroy({ where: { id } })

      return data
    }
  } else {
    throw new Error('Operation denied.')
  }
}
