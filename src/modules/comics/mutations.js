// Imports
import { GraphQLString, GraphQLInt } from 'graphql'

// App Imports
import Comics from './types'
import { create, update, remove } from './resolvers'

// Comics create
export const artistCreate = {
  type: Comics,
  args: {
    image: {
      name: 'image',
      type: GraphQLString
    },
    name: {
      name: 'name',
      type: GraphQLString
    },
    slug: {
      name: 'slug',
      type: GraphQLString
    },
    otherName: {
      name: 'otherName',
      type: GraphQLString
    },
    authorId: {
      name: 'authorId',
      type: GraphQLInt
    },
    artistId: {
      name: 'artistId',
      type: GraphQLInt
    },
    releasedYear: {
      name: 'releasedYear',
      type: GraphQLInt
    }
  },
  resolve: create
}

// Comics update
export const artistUpdate = {
  type: Comics,
  args: {
    image: {
      name: 'image',
      type: GraphQLString
    },
    name: {
      name: 'name',
      type: GraphQLString
    },
    slug: {
      name: 'slug',
      type: GraphQLString
    },
    otherName: {
      name: 'otherName',
      type: GraphQLString
    },
    authorId: {
      name: 'authorId',
      type: GraphQLInt
    },
    artistId: {
      name: 'artistId',
      type: GraphQLInt
    },
    releasedYear: {
      name: 'releasedYear',
      type: GraphQLInt
    }
  },
  resolve: update
}
// Comics remove
export const artistRemove = {

  type: Comics,
  args: {
    id: {
      name: 'id',
      type: GraphQLInt
    }
  },
  resolve: remove
}
