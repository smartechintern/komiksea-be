// Product
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('tbl_comics', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    image: {
      allowNull: true,
      type: DataTypes.STRING(191),
      get () {
        return `http://localhost:3232${this.getDataValue('image')}`
      }
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING(191)
    },
    slug: {
      allowNull: false,
      type: DataTypes.STRING(191)
    },
    otherName: {
      allowNull: false,
      type: DataTypes.STRING(191)
    },
    authorId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true,
      references: {
        model: 'tbl_author',
        key: 'id'
      },
      onUpdate: 'RESTRICT',
      onDeleted: 'RESTRICT'
    },
    artistId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true,
      references: {
        model: 'tbl_artist',
        key: 'id'
      },
      onUpdate: 'RESTRICT',
      onDeleted: 'RESTRICT'
    },
    releasedYear: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    },
    createdBy: {
      allowNull: false,
      references: {
        model: 'tbl_user',
        key: 'id'
      },
      onUpdate: 'RESTRICT',
      onDeleted: 'RESTRICT',
      type: DataTypes.INTEGER.UNSIGNED
    },
    updatedBy: {
      allowNull: true,
      defaultValue: null,
      references: {
        model: 'tbl_user',
        key: 'id'
      },
      onUpdate: 'RESTRICT',
      onDeleted: 'RESTRICT',
      type: DataTypes.INTEGER.UNSIGNED
    },
    deletedBy: {
      allowNull: true,
      defaultValue: null,
      references: {
        model: 'tbl_user',
        key: 'id'
      },
      onUpdate: 'RESTRICT',
      onDeleted: 'RESTRICT',
      type: DataTypes.INTEGER.UNSIGNED
    },
    createdAt: {
      allowNull: false,
      defaultValue: DataTypes.NOW,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.DATE
    },
    deletedAt: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.DATE
    }
  }, {
      timestamps: true,
      tableName: 'tbl_comics',
      paranoid: true
    })
}
