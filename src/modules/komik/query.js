// Imports
import { GraphQLString, GraphQLInt, GraphQLList } from 'graphql'

// App Imports
import { ProductType, ProductTypesType } from './types'
import {
  getAll, getBySlug, getById, getRelated, getTypes
} from './resolvers'

// Products All
export const komiks = {
  type: new GraphQLList(ProductType),
  resolve: getAll
}

// Product By slug
export const komik = {
  type: ProductType,
  args: {
    slug: { type: GraphQLString }
  },
  resolve: getBySlug
}

// Product By ID
export const komikById = {
  type: ProductType,
  args: {
    productId: { type: GraphQLInt }
  },
  resolve: getById
}

// Products Related
export const komiksRelated = {
  type: new GraphQLList(ProductType),
  args: {
    productId: { type: GraphQLInt }
  },
  resolve: getRelated
}

// Komik Types
export const komikTypes = {
  type: new GraphQLList(ProductTypesType),
  resolve: getTypes
}
