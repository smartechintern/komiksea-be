

// User
module.exports = function (sequelize, DataTypes) {
  let User = sequelize.define('tbl_user', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER.UNSIGNED
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING(191)
    },
    email: {
      unique: true,
      allowNull: false,
      validate: {
        isEmail: {
          msg: 'Not a valid email'
        }
      },
      type: DataTypes.STRING(191)
    },
    avatar: {
      allowNull: true,
      defaultValue: 'users/default.png',
      type: DataTypes.STRING(191)
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING(191)
    },
    remember_token: {
      allowNull: true,
      type: DataTypes.STRING(100)
    },
    createdBy: {
      allowNull: true,
      defaultValue: null,
      references: {
        model: 'tbl_user',
        key: 'id',
        as: 'createdBy'
      },
      onUpdate: 'RESTRICT',
      onDeleted: 'RESTRICT',
      type: DataTypes.INTEGER.UNSIGNED
    },
    updatedBy: {
      allowNull: true,
      defaultValue: null,
      references: {
        model: 'tbl_user',
        key: 'id',
        as: 'updatedBy'
      },
      onUpdate: 'RESTRICT',
      onDeleted: 'RESTRICT',
      type: DataTypes.INTEGER.UNSIGNED
    },
    deletedBy: {
      allowNull: true,
      defaultValue: null,
      references: {
        model: 'tbl_user',
        key: 'id',
        as: 'deletedBy'
      },
      onUpdate: 'RESTRICT',
      onDeleted: 'RESTRICT',
      type: DataTypes.INTEGER.UNSIGNED
    },
    createdAt: {
      allowNull: true,
      defaultValue: DataTypes.NOW,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.DATE
    },
    deletedAt: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.DATE
    }
  }, {
    timestamps: true,
    tableName: 'tbl_user',
    paranoid: true
  })

  return User
}
