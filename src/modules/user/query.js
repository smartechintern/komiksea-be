// Imports
import { GraphQLInt, GraphQLString, GraphQLList } from 'graphql'

// App Imports
import { UserType } from './types'
import { getAll, getById } from './resolvers'

// All
export const users = {
  type: new GraphQLList(UserType),
  args: {
    q: { type: GraphQLString },
    order: { type: GraphQLString },
    page: { type: GraphQLInt },
    pageSize: { type: GraphQLInt },
    type: { type: GraphQLString }
  },
  resolve: getAll
}

// By ID
export const user = {
  type: UserType,
  args: {
    id: { type: GraphQLInt }
  },
  resolve: getById
}
