// Imports
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import sequelize from '../../native/sequelize'

import { errorName } from '../../constants'

// App Imports
import serverConfig from '../../config/server'
import params from '../../config/params'
import models from '../../setup/models'

const Sequelize = require('sequelize')

const { Op } = Sequelize

// Create
export async function create (parentValue, { name, email, password }) {
  // Users exists with same email check
  const user = await models.User.findOne({ where: { email } })

  if (!user) {
    // User does not exists
    const passwordHashed = await bcrypt.hash(password, serverConfig.saltRounds)

    // User create
    const data = await models.User.create({
      name,
      email,
      password: passwordHashed
    })

    return data
  }
  // User exists
  throw new Error(`The email ${email} is already registered. Please try to login.`)
}

export async function login (parentValue, { email, password }) {
  const user = await models.User.findOne({ where: { email } })

  if (!user) {
    // User does not exists
    throw new Error(`We do not have any user registered with ${email} email address. Please signup.`)
  } else {
    const userDetails = user.get()

    // User exists
    const passwordMatch = await bcrypt.compare(password, userDetails.password)

    if (!passwordMatch) {
      // Incorrect password
      throw new Error('Sorry, the password you entered is incorrect. Please try again.')
    } else {
      const userDetailsToken = {
        id: userDetails.id,
        name: userDetails.name,
        email: userDetails.email,
        role: userDetails.role
      }

      return {
        user: userDetails,
        token: jwt.sign(userDetailsToken, serverConfig.secret,
          {
            expiresIn: '7d' // expires in 7 days
          })
      }
    }
  }
}

// Get by ID
export async function getById (parentValue, { id }, { auth }) {
  if (!auth.isAuthenticated && process.env.NODE_ENV === 'production') {
    throw new Error(errorName.UNAUTHORIZED)
  }

  if (auth.user && auth.user.role === params.user.roles.admin) {
    const artist = await models.Artist.findOne({ where: { id } })

    if (!artist) {
      // Artist does not exists
      throw new Error('The user does not exists.')
    } else {
      const data = await models.User.findOne({ where: { id } })

      return data
    }
  } else {
    throw new Error('Operation denied.')
  }
}

// Get all
export async function getAll (parentValue, {
  q, order, page, pageSize
}, { auth }) {
  if (!auth.isAuthenticated && process.env.NODE_ENV === 'production') {
    throw new Error(errorName.UNAUTHORIZED)
  }

  if (auth.user && auth.user.role === params.user.roles.admin) {
    const data = await models.User.findAll({
      where: {
        [Op.or]: [
          {
            name: {
              [Op.like]: `%${q}%`
            }
          },
          {
            email: {
              [Op.like]: `%${q}%`
            }
          }
        ]
      },
      order: order ? sequelize.literal(order) : [['id', 'DESC']],
      limit: parseInt(pageSize || 10, 10),
      offset: parseInt(page - 1 || 0, 0) * parseInt(pageSize || 10, 10)
    })

    return data
  }

  throw new Error('Operation denied.')
}

// Delete
export async function remove (parentValue, { id }, { auth }) {
  if (!auth.isAuthenticated && process.env.NODE_ENV === 'production') {
    throw new Error(errorName.UNAUTHORIZED)
  }

  if (auth.user && auth.user.role === params.user.roles.admin) {
    const artist = await models.Artist.findOne({ where: { id } })

    if (!artist) {
      // Artist does not exists
      throw new Error('The artist does not exists.')
    } else {
      await models.User.update({
        deletedBy: auth.user.id,
        updatedAt: sequelize.literal('updatedAt')
      }, { where: { id } })
      const data = await models.User.destroy({ where: { id } })
      return data
    }
  } else {
    throw new Error('Operation denied.')
  }
}

// User genders
export async function getGenders () {
  return Object.values(params.user.gender)
}
