## Quick start

1.  Make sure that you have Node v8 or above installed.
2.  Clone this repo using `git clone https://bitbucket.org/smartechintern/komiksea-be`
3.  Move to the appropriate directory: `cd komiksea-be`.<br />
4.  COPY /api/src/config/database.json.example to /api/src/config/database.json
5.  Modify /api/src/config/database.json
6.  Run `npm run setup` in order to install dependencies and clean the git repo.<br />
    _We auto-detect `yarn` for installing packages by default, if you wish to force `npm` usage do: `USE_YARN=false npm run setup`_<br />
    _At this point you can run `npm start` to see the example app at `http://localhost:3000`._

    _At this point you can run `npm start:prod` to runs the server
 `http://localhost:3000`._